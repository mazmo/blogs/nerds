import Head from 'next/head';
import moment from 'moment';

import Nav from './Nav';
import posts from '../../posts';

const Layout = ({ pathname, children }) => {
  const post = posts.find(post => post.urlPath === pathname);
  const title = post ? post.title : 'Mazmo para nerds';
  const description = post ? post.description : '';

  return (
    <div>
      <Nav />

      <div className="container">
        <Head>
          <title>{title}</title>
          <link rel="alternate" type="application/rss+xml" href="/public/rss-feed.xml" />
          <div dangerouslySetInnerHTML={{
            __html: `
              <meta name="image" content="https://og-image.mazmo.net/**${encodeURIComponent(title)}**.jpeg?theme=light&md=1&fontSize=100px&images=https%3A%2F%2Fmazmo.net%2Fimages%2Fisologo.png&images=https%3A%2F%2Femojipedia-us.s3.dualstack.us-west-1.amazonaws.com%2Fthumbs%2F240%2Fapple%2F271%2Fnerd-face_1f913.png" />
              <meta name="og:image" content="https://og-image.mazmo.net/**${encodeURIComponent(title)}**.jpeg?theme=light&md=1&fontSize=100px&images=https%3A%2F%2Fmazmo.net%2Fimages%2Fisologo.png&images=https%3A%2F%2Femojipedia-us.s3.dualstack.us-west-1.amazonaws.com%2Fthumbs%2F240%2Fapple%2F271%2Fnerd-face_1f913.png" />
              <meta name="description" content="${description}" />
              <meta name="og:description" content="${description}" />
            `
          }} />
        </Head>

        <main>
          {post && (
            <div className="header">
              <h1 className="title">{post.title}</h1>
              <p className="description">{post.description}</p>
              <time dateTime={post.date}>
                {moment(post.date).format('dddd, LL')} ({moment(post.date).fromNow()})
              </time>
            </div>
          )}

          <div className="content">
            {children}
          </div>
        </main>
      </div>

      <footer>
        Copyright © {(new Date).getFullYear()} Mazmo. Todos los derechos reservados.
      </footer>

      <style jsx>{`
        .container {
          min-height: 100vh;
          padding: 0 0.5rem;
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
        }

        footer {
          font-size: .875rem;
          background: #fafafa;
          border-top: 1px solid #eaeaea;
          padding: 24px;
          color: #666;
          width: 100%;
        }
      `}</style>

      <style jsx global>{`
        html,
        body {
          padding: 0;
          margin: 0;
          font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto,
            Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,
            sans-serif;
        }

        * {
          box-sizing: border-box;
        }

        a {
          color: #ED4D3D;
          text-decoration: none;
        }

        .header {
          text-align: center;
          margin-bottom: 2em;
        }

        .header time {
          font-size: 14px;
          color: #666;
          font-family: "Inter",-apple-system,BlinkMacSystemFont,"Segoe UI","Roboto","Oxygen","Ubuntu","Cantarell","Fira Sans","Droid Sans","Helvetica Neue",sans-serif;
        }

        main {
          padding: 5rem 0;
          flex: 1;
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
        }

        .title a:hover,
        .title a:focus,
        .title a:active {
          text-decoration: underline;
        }

        .title {
          margin: 0;
          line-height: 1.15;
          font-size: 4rem;
        }

        .title,
        .description {
          text-align: center;
        }

        .description {
          line-height: 1.5;
          font-size: 1.5rem;
          margin: 0.5em 0 0;
        }

        .description .highlight {
          background: #F5EFEF;
          border-radius: 5px;
          padding: 0.5rem;
          white-space: nowrap;
        }

        .content {
          max-width: 768px;
          margin: 30px auto 100px;
          font-size: 18px;
        }

        .content code {
          background: #E2E2E2;
          padding: 2px 4px;
          border-radius: 5px;
        }

        .content pre {
          font-size: 14px;
          min-width: 150px;
          white-space: break-spaces;
        }

        .content pre > code {
          background: #E2E2E2;
          padding: 8px;
          border-radius: 5px;
          display: block;
        }

        .content pre > code.language-warning {
          background-color: #FFEDA7;
          border: 1px solid #FFDD58;
        }

        .content img {
          max-width: 100%;
        }

        h2, h3, h4, h5, h6 {
          margin-top: 2em;
        }
      `}</style>
    </div>
  );
};

export default Layout;
