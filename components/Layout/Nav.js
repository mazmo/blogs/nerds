import { useRouter } from 'next/router';

import Isologo from './Isologo';
import GitLab from './GitLab';
import Chat from './Chat';

const Nav = () => {
  const router = useRouter();

  const scrollToTop = () => {
    if (document.body.scrollTop !== 0 || document.documentElement.scrollTop !== 0) {
        window.scrollBy(0, -150);
        requestAnimationFrame(scrollToTop);
    }
  };

  const onLogoClick = () => {
    if (document.body.scrollTop !== 0 || document.documentElement.scrollTop !== 0) {
      scrollToTop();
    } else {
      router.push('/');
    }
  };

  return (
    <>
      <nav>
        <div>
          <div>
            <Isologo onClick={onLogoClick} />
          </div>

          <div>
            <a href="https://mazmo.net/chat/channels/5dc991db1dcf380025464021">
              <Chat />
            </a>
            <a href="https://gitlab.com/mazmo/webapp">
              <GitLab />
            </a>
          </div>
        </div>
      </nav>

      <style jsx>{`
        nav {
          display: flex;
          flex-direction: column;
          padding: 0 32px;
          position: sticky;
          top: 0;
          display: flex;
          justify-content: center;
          width: 100%;
          max-width: 100%;
          background-color: hsla(0,0%,100%,0.8);
          z-index: 101;
          min-height: 64px;
          transition: box-shadow .1s ease 0s;
          box-shadow: inset 0 -1px 0 0 rgba(0,0,0,0.1);
          backdrop-filter: saturate(180%) blur(5px);
        }

        nav > div {
          display: flex;
          justify-content: space-between;
          flex-direction: row;
        }

        nav > div > div {
          display: flex;
          justify-content: center;
        }

        nav > div > div > a {
          width: 35px;
          height: 35px;
          opacity: 0.3;
          transition: all .1s ease 0s;
          display: block;
          text-align: center;
          margin-left: 8px;
          display: flex;
          flex-direction: column;
          justify-content: center;
        }

        nav > div > div > a:hover {
          opacity: 1;
        }
      `}</style>
    </>
  );
};

export default Nav;
